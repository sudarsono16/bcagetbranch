package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import model.mdlAPIResult;
import model.mdlLog;

import org.apache.logging.log4j.LogManager;

public class DeviceManagementAdapter {
    final static Logger logger = LogManager.getLogger(DeviceManagementAdapter.class);
    static Gson gson = new Gson();
    static Client client = Client.create();

    public static Integer CheckWSID(String SerialNumber, String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getBranchList";
	mdlLog.SystemFunction = functionName;
	mdlLog.LogSource = "Webservice";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Failed";
	mdlLog.ErrorMessage = "";

	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Integer returnValue = 0;
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT SerialNumber FROM ms_device WHERE WSID = ?";
	    pstm = connection.prepareStatement(sql);
	    pstm.setString(1, WSID);
	    jrs = pstm.executeQuery();
	    while (jrs.next()) {
		String SerialNumberFromDB = jrs.getString("SerialNumber");
		// if serial number is not same, then return error
		if (!SerialNumber.equalsIgnoreCase(SerialNumberFromDB)) {
		    returnValue = 1;
		}
	    }
	    mdlLog.LogStatus = "Success";
	} catch (Exception ex) {
	    returnValue = 2;
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", "function: " + functionName, "", "", ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	    LogAdapter.InsertLog(mdlLog);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {

	    }
	}
	return returnValue;
    }

    public static List<model.mdlBranch> GetBranchList(String branchCode, String SerialNumber, String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlBranch> branchList = new ArrayList<model.mdlBranch>();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getBranchList";
	mdlLog.SystemFunction = functionName;
	mdlLog.LogSource = "Webservice";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Failed";
	mdlLog.ErrorMessage = "";

	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT branch.BranchCode, branch.BranchName, branch.BranchTypeID, branchtype.BranchTypeName, branch.BranchInitial, "
		    + "branch.BranchCoordinator, branch.Location_Type, branch.Kanwil, branch.Address, branch.City, branch.Longitude, branch.Latitude, "
		    + "branch.Flag_Reservation, branch.Flag_Weekend_Banking_Saturday, branch.Flag_Weekend_Banking_Sunday,branch.Vendor_Kiosk, "
		    + "branch.Regular_Kiosk,branch.Prioritas_Kiosk,branch.Timezone FROM ms_branch branch "
		    + "LEFT JOIN ms_branchtype branchtype ON branch.BranchTypeID = branchtype.branchtypeid "
		    + "WHERE BranchCode = ?";
	    pstm = connection.prepareStatement(sql);
	    pstm.setString(1, branchCode);
	    jrs = pstm.executeQuery();
	    while (jrs.next()) {
		model.mdlBranch branch = new model.mdlBranch();
		branch.BranchCode = jrs.getString("BranchCode");
		branch.BranchName = jrs.getString("BranchName");
		branch.BranchTypeID = jrs.getString("BranchTypeID");
		branch.BranchTypeName = jrs.getString("BranchTypeName");
		branch.BranchInitial = jrs.getString("BranchInitial");
		branch.BranchCoordinator = jrs.getString("BranchCoordinator");
		branch.LocationType = jrs.getString("Location_Type");
		branch.RegionCode = jrs.getString("Kanwil");
		branch.Address = jrs.getString("Address");
		branch.City = jrs.getString("City");
		branch.Longitude = jrs.getString("Longitude");
		branch.Latitude = jrs.getString("Latitude");
		branch.FlagReservation = jrs.getString("Flag_Reservation");
		branch.FlagWeekendBankingSaturday = jrs.getString("Flag_Weekend_Banking_Saturday");
		branch.FlagWeekendBankingSunday = jrs.getString("Flag_Weekend_Banking_Sunday");
		branch.VendorKiosk = jrs.getString("Vendor_Kiosk");
		branch.RegularKiosk = jrs.getString("Regular_Kiosk");
		branch.PrioritasKiosk = jrs.getString("Prioritas_Kiosk");
		branch.Timezone = jrs.getString("Timezone");
		branchList.add(branch);
	    }
	    mdlLog.LogStatus = "Success";
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", "function: " + functionName, "", "", ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	    LogAdapter.InsertLog(mdlLog);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {

	    }
	}
	return branchList;
    }

    public static List<model.mdlBranch> searchBranchList(String search, String page, String size, String SerialNumber, String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlBranch> branchList = new ArrayList<model.mdlBranch>();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getBranchList";
	mdlLog.SystemFunction = functionName;
	mdlLog.LogSource = "Webservice";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Failed";
	mdlLog.ErrorMessage = "";

	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY branch.BranchName) rnum, branch.BranchCode, branch.BranchName, branch.BranchTypeID, "
		    + "branchtype.BranchTypeName, branch.BranchInitial, branch.Address, branch.City FROM ms_branch branch "
		    + "LEFT JOIN ms_branchtype branchtype ON branch.BranchTypeID = branchtype.branchtypeid "
		    + "WHERE UPPER(branch.BranchName) LIKE ? OR UPPER(branch.Address) LIKE ? OR UPPER(branch.City) LIKE ?) tbl "
		    + "WHERE rnum BETWEEN ? AND ? ORDER BY tbl.rnum";

	    pstm = connection.prepareStatement(sql);
	    String searchString = "";
	    if (search != null && !search.equals("")) {
		searchString = "%" + search.toUpperCase().trim().replace(" ", "%") + "%";
	    } else {
		searchString = "%%";

	    }

	    pstm.setString(1, searchString);
	    pstm.setString(2, searchString);
	    pstm.setString(3, searchString);

	    int startRow = ((Integer.parseInt(page) - 1) * Integer.parseInt(size)) + 1;
	    int endRow = Integer.parseInt(page) * Integer.parseInt(size);
	    pstm.setInt(4, startRow);
	    pstm.setInt(5, endRow);

	    jrs = pstm.executeQuery();
	    while (jrs.next()) {
		model.mdlBranch branch = new model.mdlBranch();
		branch.BranchCode = jrs.getString("BranchCode");
		branch.BranchName = jrs.getString("BranchName");
		branch.BranchTypeID = jrs.getString("BranchTypeID");
		branch.BranchTypeName = jrs.getString("BranchTypeName");
		branch.BranchInitial = jrs.getString("BranchInitial");
		branch.Address = jrs.getString("Address");
		branch.City = jrs.getString("City");
		branchList.add(branch);
	    }
	    mdlLog.LogStatus = "Success";
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", "function: " + functionName, "", "", ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	    LogAdapter.InsertLog(mdlLog);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {

	    }
	}
	return branchList;
    }

    public static String GetBranchTypeName(String branchTypeID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String branchTypeName = "";
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getBranchList";
	mdlLog.SystemFunction = "GetBranchTypeName";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";
	mdlLog.ErrorMessage = "";
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT BranchTypeName FROM ms_branchtype WHERE branchtypeid = ?";
	    pstm = connection.prepareStatement(sql);
	    pstm.setString(1, branchTypeID);
	    jrs = pstm.executeQuery();
	    while (jrs.next()) {
		branchTypeName = jrs.getString("BranchTypeName");
	    }
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", "function: " + functionName, "", "", ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	    LogAdapter.InsertLog(mdlLog);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {

	    }
	}
	return branchTypeName;
    }

    public static mdlAPIResult HitAPIBranchInquiryEAI(String branchCode, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	String systemFunction = Thread.currentThread().getStackTrace()[1].getMethodName();
	String apiFunction = "BranchInquiry";
	mdlAPIResult mdlBranchInquiryResult = new mdlAPIResult();
	mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = apiFunction;
	mdlLog.SystemFunction = systemFunction;
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";
	String jsonOut = "";
	String urlAPI = "/branch/v2/branch-codes/" + branchCode;
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_key_api");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);
	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlAPI, "GET", "function : " + systemFunction + ", client-id: " + decryptedClientID
		    + ", branchCode :" + branchCode, "", jsonOut));
	    mdlBranchInquiryResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlBranchInquiryResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlBranchInquiryResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlBranchInquiryResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlBranchInquiryResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, urlAPI, "GET", "function : " + systemFunction + ", branchCode :"
		    + branchCode, "", jsonOut, ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlBranchInquiryResult;
    }
}