package adapter;

import model.mdlErrorSchema;
import model.mdlMessage;

public class ErrorAdapter {
    public static model.mdlErrorSchema GetErrorSchema(String errorCode) {
	mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	mdlMessage mdlMessage = new model.mdlMessage();
	switch (errorCode) {
	case "00":
	    mdlErrorSchema.ErrorCode = "00";
	    mdlMessage.Indonesian = "Sukses";
	    mdlMessage.English = "Success";
	    break;
	case "01":
	    mdlErrorSchema.ErrorCode = "01";
	    mdlMessage.Indonesian = "WSID yang Anda input sudah terdaftar";
	    mdlMessage.English = "Your WSID Input Already Registered";
	    break;
	case "02":
	    mdlErrorSchema.ErrorCode = "02";
	    mdlMessage.Indonesian = "Pengecekan WSID gagal";
	    mdlMessage.English = "WSID check failed";
	    break;
	case "03":
	    mdlErrorSchema.ErrorCode = "03";
	    mdlMessage.Indonesian = "Cabang tidak ditemukan";
	    mdlMessage.English = "Branch not found";
	    break;
	case "04":
	    mdlErrorSchema.ErrorCode = "04";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = "Service call failed";
	    break;
	case "05":
	    mdlErrorSchema.ErrorCode = "05";
	    mdlMessage.Indonesian = "Gagal memanggil service HitAPIBranchInquiryEAI";
	    mdlMessage.English = "Service HitAPIBranchInquiryEAI call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case "06":
	    mdlErrorSchema.ErrorCode = "06";
	    mdlMessage.Indonesian = "Data cabang EAI kosong.";
	    mdlMessage.English = "EAI branch data is empty.";
	    break;
	default:
	    mdlErrorSchema.ErrorCode = "04";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = "Service call failed";
	    break;
	}
	mdlErrorSchema.ErrorMessage = mdlMessage;
	return mdlErrorSchema;
    }
}
