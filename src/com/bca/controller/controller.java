package com.bca.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.DeviceManagementAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import model.mdlAPIResult;
import model.mdlBranch;
import model.mdlBranchList;
import model.mdlErrorSchema;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody void GetPing() {
	return;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody model.mdlAPIResult GetBranchList(@RequestParam(value = "serial", defaultValue = "") String serialNumber,
	    @RequestParam(value = "wsid", defaultValue = "") String wsid, @RequestParam(value = "source", defaultValue = "db") String dataSouce,
	    @RequestParam(value = "branch-code", defaultValue = "", required = false) String g2Branch) {
	long startTime = System.currentTimeMillis();
	mdlAPIResult mdlGetBranchListResult = new mdlAPIResult();
	mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = wsid;
	mdlLog.SerialNumber = serialNumber;
	mdlLog.ApiFunction = "getBranchList";
	mdlLog.SystemFunction = "GetBranchList";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	String customData = "serial:" + serialNumber + ", wsid : " + wsid + ", datasource :" + dataSouce + ", branchCode :" + g2Branch;

	// Integer checkWSID = 1;
	try {
	    // checkWSID = DeviceManagementAdapter.CheckWSID(SerialNumber, WSID);
	    // if (checkWSID == 1) {
	    // mdlErrorSchema.ErrorCode = "01";
	    // mdlMessage.Indonesian = "WSID yang Anda input sudah terdaftar";
	    // mdlMessage.English = mdlLog.ErrorMessage = "Your WSID Input Already Registered";
	    // mdlErrorSchema.ErrorMessage = mdlMessage;
	    // mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
	    // LogAdapter.InsertLog(mdlLog);
	    // return mdlGetBranchListResult;
	    // } else if (checkWSID == 2) {
	    // mdlErrorSchema.ErrorCode = "02";
	    // mdlMessage.Indonesian = "Pengecekan WSID gagal";
	    // mdlMessage.English = mdlLog.ErrorMessage = "WSID check failed";
	    // mdlErrorSchema.ErrorMessage = mdlMessage;
	    // mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
	    // LogAdapter.InsertLog(mdlLog);
	    // return mdlGetBranchListResult;
	    // }
	    String branchCode = "";
	    if (!g2Branch.equalsIgnoreCase("")) {
		branchCode = g2Branch;
		wsid = g2Branch;
	    } else if (g2Branch.equalsIgnoreCase("")) {
		branchCode = wsid.substring(0, 4);
	    }
	    List<mdlBranch> branchList = new ArrayList<mdlBranch>();

	    if (dataSouce.equalsIgnoreCase("eai")) {
		mdlAPIResult getEAIBranchResult = DeviceManagementAdapter.HitAPIBranchInquiryEAI(branchCode, serialNumber, wsid);
		if (getEAIBranchResult == null || getEAIBranchResult.ErrorSchema == null) {
		    mdlErrorSchema = ErrorAdapter.GetErrorSchema("05");
		    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
		    mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
		    logger.error(LogAdapter.logToLog4j(false, startTime, 400, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlGetBranchListResult)));
		    LogAdapter.InsertLog(mdlLog);
		    return mdlGetBranchListResult;
		} else {
		    String getEAIBranchJson = gson.toJson(getEAIBranchResult);
		    if (!getEAIBranchResult.ErrorSchema.ErrorCode.equals("ESB-00-000")) {
			mdlErrorSchema = getEAIBranchResult.ErrorSchema;
			mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
			mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
			logger.error(LogAdapter.logToLog4j(false, startTime, 400, "BCAGetBranch", "GET", customData + ", getEAIBranchResult : "
				+ getEAIBranchJson, "", gson.toJson(mdlGetBranchListResult)));
			LogAdapter.InsertLog(mdlLog);
			return mdlGetBranchListResult;
		    } else {
			String branchDataString = gson.toJson(getEAIBranchResult.OutputSchema);
			mdlBranchList inquiryBranchList = gson.fromJson(branchDataString, mdlBranchList.class);
			if (inquiryBranchList == null || inquiryBranchList.branch.size() == 0) {
			    // there is no branch data in EAI
			    mdlErrorSchema = ErrorAdapter.GetErrorSchema("06");
			    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
			    mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
			    logger.error(LogAdapter.logToLog4j(false, startTime, 400, "BCAGetBranch", "GET", customData + ", getEAIBranchResult : "
				    + getEAIBranchJson, "", gson.toJson(mdlGetBranchListResult)));
			    LogAdapter.InsertLog(mdlLog);
			    return mdlGetBranchListResult;
			} else {
			    for (mdlBranch branch : inquiryBranchList.branch) {
				branch.BranchCode = branch.BranchCode.substring(0, 4);
				branch.BranchTypeName = DeviceManagementAdapter.GetBranchTypeName(branch.BranchTypeID);
				branchList.add(branch);
			    }
			}
		    }
		}
	    } else {
		branchList = DeviceManagementAdapter.GetBranchList(branchCode, serialNumber, wsid);
	    }

	    if (branchList == null || branchList.size() == 0) {
		mdlErrorSchema = ErrorAdapter.GetErrorSchema("03");
		mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
		mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
		logger.error(LogAdapter.logToLog4j(false, startTime, 400, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlGetBranchListResult)));
		LogAdapter.InsertLog(mdlLog);
		return mdlGetBranchListResult;
	    } else {
		mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
		mdlLog.ErrorMessage = mdlLog.LogStatus = mdlErrorSchema.ErrorMessage.English;
		mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
		mdlGetBranchListResult.OutputSchema = branchList;
	    }
	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlGetBranchListResult)));
	} catch (Exception ex) {
	    mdlErrorSchema = ErrorAdapter.GetErrorSchema("04");
	    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
	    mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlGetBranchListResult), ex.toString()), ex);
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlGetBranchListResult;
    }

    @RequestMapping(value = "/inquiry-branch", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String inquiryBranch(@RequestParam(value = "search", required = true) String searchString,
	    @RequestParam(value = "serial", defaultValue = "") String serialNumber, @RequestParam(value = "wsid", defaultValue = "") String wsid,
	    @RequestParam(value = "page", defaultValue = "1") String page, @RequestParam(value = "size", defaultValue = "4") String size) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();

	mdlAPIResult mdlInquiryBranchResult = new mdlAPIResult();
	mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = wsid;
	mdlLog.SerialNumber = serialNumber;
	mdlLog.ApiFunction = "inquiryBranch";
	mdlLog.SystemFunction = functionName;
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	String customData = "search: " + searchString + ", serial:" + serialNumber + ", wsid : " + wsid;

	try {
	    List<mdlBranch> branchList = new ArrayList<mdlBranch>();
	    branchList = DeviceManagementAdapter.searchBranchList(searchString, page, size, serialNumber, wsid);
	    if (branchList == null || branchList.size() == 0) {
		mdlErrorSchema = ErrorAdapter.GetErrorSchema("03");
		mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
		mdlInquiryBranchResult.ErrorSchema = mdlErrorSchema;
		logger.error(LogAdapter.logToLog4j(false, startTime, 400, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlInquiryBranchResult)));
		LogAdapter.InsertLog(mdlLog);
		return gson.toJson(mdlInquiryBranchResult);
	    } else {
		mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
		mdlLog.ErrorMessage = mdlLog.LogStatus = mdlErrorSchema.ErrorMessage.English;
		mdlInquiryBranchResult.ErrorSchema = mdlErrorSchema;
		mdlInquiryBranchResult.OutputSchema = branchList;
	    }

	} catch (Exception ex) {
	    mdlErrorSchema = ErrorAdapter.GetErrorSchema("04");
	    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
	    mdlInquiryBranchResult.ErrorSchema = mdlErrorSchema;
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetBranch", "GET", customData, "", gson.toJson(mdlInquiryBranchResult), ex.toString()), ex);
	}
	LogAdapter.InsertLog(mdlLog);
	return gson.toJson(mdlInquiryBranchResult);

    }
}
