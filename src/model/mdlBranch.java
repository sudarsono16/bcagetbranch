package model;

import com.google.gson.annotations.SerializedName;

public class mdlBranch {
    @SerializedName(value = "BranchCode", alternate = "branch_code")
    public String BranchCode;
    @SerializedName(value = "BranchName", alternate = "branch_name")
    public String BranchName;
    @SerializedName(value = "BranchTypeID", alternate = "branch_type")
    public String BranchTypeID;
    @SerializedName(value = "BranchTypeName", alternate = "branch_type_name")
    public String BranchTypeName;
    @SerializedName(value = "BranchInitial", alternate = "branch_initial")
    public String BranchInitial;
    @SerializedName(value = "BranchCoordinator", alternate = "branch_coordinator")
    public String BranchCoordinator;
    @SerializedName(value = "LocationType", alternate = "location_type")
    public String LocationType;
    @SerializedName(value = "RegionCode", alternate = "region_code")
    public String RegionCode;
    @SerializedName(value = "Address", alternate = "address")
    public String Address;
    @SerializedName(value = "City", alternate = "city")
    public String City;
    @SerializedName(value = "Longitude", alternate = "longitude")
    public String Longitude;
    @SerializedName(value = "Latitude", alternate = "latitude")
    public String Latitude;
    @SerializedName(value = "FlagReservation", alternate = "flag_reservation")
    public String FlagReservation;
    @SerializedName(value = "FlagWeekendBankingSaturday", alternate = "flag_weekend_banking_saturday")
    public String FlagWeekendBankingSaturday;
    @SerializedName(value = "FlagWeekendBankingSunday", alternate = "flag_weekend_banking_sunday")
    public String FlagWeekendBankingSunday;
    @SerializedName(value = "VendorKiosk", alternate = "vendor_kiosk")
    public String VendorKiosk;
    @SerializedName(value = "RegularKiosk", alternate = "regular_kiosk")
    public String RegularKiosk;
    @SerializedName(value = "PrioritasKiosk", alternate = "prioritas_kiosk")
    public String PrioritasKiosk;
    @SerializedName(value = "Timezone", alternate = "timezone")
    public String Timezone;
}
